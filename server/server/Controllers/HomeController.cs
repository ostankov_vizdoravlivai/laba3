﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Net.Http;

namespace server.Controllers
{
    public class HomeController : Controller
    {
        dbworker a = new dbworker();
        //log/reg
        public class for_auto_resp
        {
            public int code { get; set; }
            public string token { get; set; }
        }
        [HttpPost]
        public string auto()
        {
            string login = Request.Params.Get("login");
            string pass = Request.Params.Get("pass");
            string token = dbworker.get_token(login, pass);
            for_auto_resp resp = new for_auto_resp();
            if (token == "")
                resp.code = 405;
            else
            {
                resp.code = 200;
                resp.token = token;
            }
            return JsonConvert.SerializeObject(resp);
        }
        public class for_reg_resp
        {
            public int code { get; set; }
            public int key { get; set; }
        }
        [HttpPost]
        public string reg(string a)
        {
            string login = Request.Params.Get("login");
            string pass = Request.Params.Get("pass");
            string mail = Request.Params.Get("mail");
            for_reg_resp resp = new for_reg_resp();
            if (dbworker.reg(login, pass, mail)){
                resp.code = 200;
                resp.key = 109875;
            }
            else resp.code = 405;
            return JsonConvert.SerializeObject(resp);
        }
        public class for_regkey
        {
            public string mail { get; set; }
            public int key { get; set; }
        }
        [HttpPost]
        public string regkey(string a)
        {
            for_regkey request = JsonConvert.DeserializeObject<for_regkey>(a);
            for_auto_resp resp = new for_auto_resp();
            string token = dbworker.mail(request.mail, request.key);
            if(token!="")
            {
                resp.code = 200;
                resp.token = token;
            }
            else resp.code = 405;
            return JsonConvert.SerializeObject(resp);
        }
        public class code_r
        {
            public int code { get; set; }
        }
        //users_photos
        public string cities()
        {
            return JsonConvert.SerializeObject(dbworker.getcities());
        }
        public string viewer(string city)
        {
            return JsonConvert.SerializeObject(dbworker.get_photo_by_city(city));
        }
        public string rating(string mail, string token, string id, string rating)
        {
            code_r t = new code_r();
            if (dbworker.rate(mail, token, System.Convert.ToInt32(id), System.Convert.ToSingle(rating)))
                t.code = 200;
            else t.code = 405;
            return JsonConvert.SerializeObject(t);
        }
        public string saving(string mail, string token, string id)
        {
            code_r t = new code_r();
            if (dbworker.add_to_book(mail, token, System.Convert.ToInt32(id)))
                t.code = 200;
            else t.code = 405;
            return JsonConvert.SerializeObject(t);
        }
        //my_photos
        public string saved(string mail, string token)
        {
            return JsonConvert.SerializeObject(dbworker.get_saved(mail, token));
        }
        public string delete(string mail, string token, string id)
        {
            code_r t = new code_r();
            if (dbworker.delete(mail, token,System.Convert.ToInt32(id)))
                t.code = 200;
            else t.code = 405;
            return JsonConvert.SerializeObject(t);
        }
        public class for_put
        {
            public string mail { get; set; }
            public string token { get; set; }
            public string photo { get; set; }
        }
        [HttpPost]
        public string put(string a)
        {
            for_put p = JsonConvert.DeserializeObject<for_put>(a);
            code_r t = new code_r();
            if (dbworker.put(p.mail, p.token, p.photo))
                t.code = 200;
            else t.code = 405;
            return JsonConvert.SerializeObject(t);
        }
    }

}