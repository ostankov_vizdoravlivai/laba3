﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace server.Controllers
{
    public class dbworker
    {
        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        static SqlCommand com;
        static SqlDataReader read;
        public class for_viewer
        {
            public for_viewer(string a, float b, string c, int d)
            {
                nick = a;rating = b;url = c; id = d;
            }
            public string nick { get; set; }
            public float rating { get; set; }
            public string url { get; set; }
            public int id { get; set; }
        }

        public static string get_token(string l, string p)
        {
            string result = "";
            try
            {
                con.Open();
                com = new SqlCommand("select * from [user] where [user].login=@l and [user].pass=@p",con);
                com.Parameters.AddWithValue("l", l);
                com.Parameters.AddWithValue("p", p);
                read = com.ExecuteReader();
                if (read.Read()) { result += l + "|" + p; }
                read.Close();
                con.Close();
            }
            catch (Exception Ex) {
                con.Close();
            }
            return result;
        }
        public class users_reg
        {
            public users_reg(string login, string pass, string mail)
            {
                this.login = login; this.pass = pass; this.mail = mail;
            }
           public string login { get; set; }
            public string pass { get; set; }
            public string mail { get; set; }
        }
        static List<users_reg> list = new List<users_reg>();
        public static bool reg(string l, string p, string m)
        {
            bool result = true;
            try
            {
                con.Open();
                com = new SqlCommand("select * from [user] where [user].mail=@m", con);
                com.Parameters.AddWithValue("m", m);
                read = com.ExecuteReader();
                if (read.Read()) { result = false; }
                read.Close();
                con.Close();
            }
            catch (Exception Ex)
            {
                result = false;
                con.Close();
            }
            if (result) list.Add(new users_reg(l, p, m));
            return result;
        }
        public static string mail(string m, int k)
        {
            if(list.Exists(x=>x.mail==m) && k == 109875)
            {
                foreach(var i in list)
                {
                    if (i.mail == m)
                    {
                        con.Open();
                        com = new SqlCommand("insert into [users] (login,pass,mail) values(@l,@p,@m)", con);
                        com.Parameters.AddWithValue("l", i.login);
                        com.Parameters.AddWithValue("p", i.pass);
                        com.Parameters.AddWithValue("m", m);
                        read = com.ExecuteReader();
                        con.Close();
                        return get_token(i.login, i.pass);
                    }
                }
            }
            return "";
        }
        public static List<string> getcities()
        {
            con.Open();
            com = new SqlCommand("select city from photo", con);
            read = com.ExecuteReader();
            List<string> result = new List<string>();
            while (read.Read())
            {
                string buff = (string)read["city"];
                if (!result.Exists(x => x == buff)) result.Add(buff);
            }
            read.Close();
            con.Close();
            return result;
        }
        public static List<for_viewer> get_photo_by_city(string c)
        {
            con.Open();
            com = new SqlCommand("select * from photo where photo.city=@c", con);
            com.Parameters.AddWithValue("c", c);
            read = com.ExecuteReader();
            List<for_viewer> result = new List<for_viewer>();
            while (read.Read())
            {
                result.Add(new for_viewer((string)read["nick"], System.Convert.ToSingle(read["rating"]), (string)read["url"], (int)read["id"]));
            }
            read.Close();
            con.Close();
            return result;
        }
        public static  bool check_token(string m, string token)
        {
            con.Open();
            com = new SqlCommand("select * from [user] where [user].mail=@m", con);
            com.Parameters.AddWithValue("m", m);
            read = com.ExecuteReader();
            string l="", p="";
            if (read.Read()) {
                l = (string)read["login"];
                p = (string)read["pass"];
            }
            read.Close();
            con.Close();
            if (get_token(l, p) != "" && get_token(l, p)==token) return true;
            else return false;
        }
        public static bool rate(string m, string t, int i, float r)
        {
            bool result = false;
            if (check_token(m, t))
            {
                con.Open();
                com = new SqlCommand("select rating, rated from photo where photo.id=@id", con);
                com.Parameters.AddWithValue("id", i);
                read = com.ExecuteReader();
                int rated = 0;
                float rat = 0.0f;
                if (read.Read())
                {
                    result = true;
                    rated = System.Convert.ToInt32(read["rated"]);
                    rat = System.Convert.ToSingle(read["rating"]);
                }
                read.Close();
                com = new SqlCommand("update photo set photo.rated=@rated, photo.rating = @rating where photo.id=@id", con);
                com.Parameters.AddWithValue("id", i);
                com.Parameters.AddWithValue("rated", rated+1);
                com.Parameters.AddWithValue("rating", (rat/rated+r)/(rated+1.0));
                com.ExecuteNonQuery();
                con.Close();
            }
            return result;
        }
        public static bool add_to_book(string m, string t, int i)
        {
            if (check_token(m, t))
            {
                con.Open();
                com = new SqlCommand("select id from [user] where [user].mail=@m", con);
                com.Parameters.AddWithValue("m", m);
                read = com.ExecuteReader();
                read.Read();
                int id = System.Convert.ToInt32(read["id"]);
                read.Close();
                com = new SqlCommand("insert into saved (photo_id, user_id) values (@a,@b)");
                com.Parameters.AddWithValue("a", i);
                com.Parameters.AddWithValue("b", id);
                com.ExecuteNonQuery();
                con.Close();
                return true;

            }
            return false; ;
        }
        public static List<for_viewer> get_saved(string m, string t)
        {
            if (check_token(m, t))
            {
                con.Open();
                com = new SqlCommand("select id from [user] where [user].mail=@m", con);
                com.Parameters.AddWithValue("m", m);
                read = com.ExecuteReader();
                read.Read();
                int id = System.Convert.ToInt32(read["id"]);
                read.Close();
                com = new SqlCommand("select photo_id from saved where saved.user_id=@m", con);
                com.Parameters.AddWithValue("m", id);
                List<int> ids = new List<int>();
                while (read.Read())
                {
                    if (!ids.Exists(x => x == System.Convert.ToInt32(read["photo_id"]))) ids.Add(System.Convert.ToInt32(read["photo_id"]));
                }
                read.Close();
                List<for_viewer> res = new List<for_viewer>();
                foreach (var i in ids)
                {
                    com = new SqlCommand("select * from photo where photo.id=@m", con);
                    com.Parameters.AddWithValue("m", id);
                    read = com.ExecuteReader();
                    while (read.Read())
                    {
                        res.Add(new for_viewer((string)read["nick"], System.Convert.ToSingle(read["rating"]), (string)read["url"], (int)read["id"]));
                    }
                    read.Close();
                }
                con.Close();
                return res;
            }
            return new List<for_viewer>();
        }
        public static bool delete(string m, string t, int i)
        {
            if (check_token(m, t))
            {
                con.Open();
                com = new SqlCommand("select nick from [user] where [user].mail=@m", con);
                com.Parameters.AddWithValue("m", m);
                read = com.ExecuteReader();
                read.Read();
                string nick = (string)read["nick"];
                read.Close();
                com = new SqlCommand("select * from photo where photo.nick=@n and photo.id=@i", con);
                com.Parameters.AddWithValue("n", nick);
                com.Parameters.AddWithValue("i", i);
                read = com.ExecuteReader();
                if (!read.Read())
                {
                    read.Close();
                    con.Close();
                    return false;
                }
                else read.Close();
                com = new SqlCommand("selete from photo where photo.id=@i", con);
                com.Parameters.AddWithValue("i", i);
                com.ExecuteNonQuery();
                con.Close();
                return true;
            }
            return false;
        }
        public static bool put(string m, string t, string u)
        {
            if (check_token(m, t))
            {
                con.Open();
                com = new SqlCommand("select nick from [user] where [user].mail=@m", con);
                com.Parameters.AddWithValue("m", m);
                read = com.ExecuteReader();
                read.Read();
                string nick = (string)read["nick"];
                read.Close();
                com = new SqlCommand("insert into photo (nick,url) values (@n,@u)", con);
                com.Parameters.AddWithValue("nick", nick);
                com.Parameters.AddWithValue("u", u);
                com.ExecuteNonQuery();
                con.Close();
                return true;
            }
            return false;
        }
    }
}