package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;


@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaInflectorServerCodegen", date = "2019-05-03T17:25:47.450Z[GMT]")public class InlineResponse2002   {
  @JsonProperty("city")
  private String city = null;
  @JsonProperty("kod")
  private Integer kod = null;
  /**
   * Город
   **/
  public InlineResponse2002 city(String city) {
    this.city = city;
    return this;
  }

  
  @Schema(description = "Город")
  @JsonProperty("city")
  public String getCity() {
    return city;
  }
  public void setCity(String city) {
    this.city = city;
  }

  /**
   * Код
   **/
  public InlineResponse2002 kod(Integer kod) {
    this.kod = kod;
    return this;
  }

  
  @Schema(description = "Код")
  @JsonProperty("kod")
  public Integer getKod() {
    return kod;
  }
  public void setKod(Integer kod) {
    this.kod = kod;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse2002 inlineResponse2002 = (InlineResponse2002) o;
    return Objects.equals(city, inlineResponse2002.city) &&
        Objects.equals(kod, inlineResponse2002.kod);
  }

  @Override
  public int hashCode() {
    return Objects.hash(city, kod);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse2002 {\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    kod: ").append(toIndentedString(kod)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
