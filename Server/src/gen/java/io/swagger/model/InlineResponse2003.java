package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;


@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaInflectorServerCodegen", date = "2019-05-03T17:25:47.450Z[GMT]")public class InlineResponse2003   {
  @JsonProperty("nick")
  private String nick = null;
  @JsonProperty("rating")
  private Integer rating = null;
  @JsonProperty("url")
  private String url = null;
  @JsonProperty("id")
  private Integer id = null;
  /**
   * Ник владельца
   **/
  public InlineResponse2003 nick(String nick) {
    this.nick = nick;
    return this;
  }

  
  @Schema(description = "Ник владельца")
  @JsonProperty("nick")
  public String getNick() {
    return nick;
  }
  public void setNick(String nick) {
    this.nick = nick;
  }

  /**
   * Оценка
   **/
  public InlineResponse2003 rating(Integer rating) {
    this.rating = rating;
    return this;
  }

  
  @Schema(description = "Оценка")
  @JsonProperty("rating")
  public Integer getRating() {
    return rating;
  }
  public void setRating(Integer rating) {
    this.rating = rating;
  }

  /**
   * Ссылка
   **/
  public InlineResponse2003 url(String url) {
    this.url = url;
    return this;
  }

  
  @Schema(description = "Ссылка")
  @JsonProperty("url")
  public String getUrl() {
    return url;
  }
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   * Идентификатор картинки
   **/
  public InlineResponse2003 id(Integer id) {
    this.id = id;
    return this;
  }

  
  @Schema(description = "Идентификатор картинки")
  @JsonProperty("id")
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse2003 inlineResponse2003 = (InlineResponse2003) o;
    return Objects.equals(nick, inlineResponse2003.nick) &&
        Objects.equals(rating, inlineResponse2003.rating) &&
        Objects.equals(url, inlineResponse2003.url) &&
        Objects.equals(id, inlineResponse2003.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nick, rating, url, id);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse2003 {\n");
    sb.append("    nick: ").append(toIndentedString(nick)).append("\n");
    sb.append("    rating: ").append(toIndentedString(rating)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
