/*
 * First step
 * description
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;

/**
 * InlineResponse2002
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2019-05-03T17:21:49.098Z[GMT]")public class InlineResponse2002 {

  @SerializedName("city")
  private String city = null;

  @SerializedName("kod")
  private Integer kod = null;
  public InlineResponse2002 city(String city) {
    this.city = city;
    return this;
  }

  

  /**
  * Город
  * @return city
  **/
  @Schema(description = "Город")
  public String getCity() {
    return city;
  }
  public void setCity(String city) {
    this.city = city;
  }
  public InlineResponse2002 kod(Integer kod) {
    this.kod = kod;
    return this;
  }

  

  /**
  * Код
  * @return kod
  **/
  @Schema(description = "Код")
  public Integer getKod() {
    return kod;
  }
  public void setKod(Integer kod) {
    this.kod = kod;
  }
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse2002 inlineResponse2002 = (InlineResponse2002) o;
    return Objects.equals(this.city, inlineResponse2002.city) &&
        Objects.equals(this.kod, inlineResponse2002.kod);
  }

  @Override
  public int hashCode() {
    return java.util.Objects.hash(city, kod);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse2002 {\n");
    
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    kod: ").append(toIndentedString(kod)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
