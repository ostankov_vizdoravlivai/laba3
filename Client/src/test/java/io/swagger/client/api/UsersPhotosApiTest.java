/*
 * First step
 * description
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.InlineResponse2002;
import io.swagger.client.model.InlineResponse2003;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for UsersPhotosApi
 */
@Ignore
public class UsersPhotosApiTest {

    private final UsersPhotosApi api = new UsersPhotosApi();

    /**
     * Получение списка городов
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void mainCitiesGetTest() throws ApiException {
        List<InlineResponse2002> response = api.mainCitiesGet();

        // TODO: test validations
    }
    /**
     * Получение фотографий пользователей
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void mainViewerGetTest() throws ApiException {
        Integer city = null;
        List<InlineResponse2003> response = api.mainViewerGet(city);

        // TODO: test validations
    }
    /**
     * Оценка фотографии
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void mainViewerRatingGetTest() throws ApiException {
        String mail = null;
        String token = null;
        Integer id = null;
        Integer rating = null;
        api.mainViewerRatingGet(mail, token, id, rating);

        // TODO: test validations
    }
    /**
     * Сохранение фотографии
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void mainViewerSavingGetTest() throws ApiException {
        String mail = null;
        String token = null;
        Integer id = null;
        api.mainViewerSavingGet(mail, token, id);

        // TODO: test validations
    }
}
