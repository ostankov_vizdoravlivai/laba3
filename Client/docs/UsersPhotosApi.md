# UsersPhotosApi

All URIs are relative to *https://api.example.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**mainCitiesGet**](UsersPhotosApi.md#mainCitiesGet) | **GET** /main/cities | Получение списка городов
[**mainViewerGet**](UsersPhotosApi.md#mainViewerGet) | **GET** /main/viewer | Получение фотографий пользователей
[**mainViewerRatingGet**](UsersPhotosApi.md#mainViewerRatingGet) | **GET** /main/viewer/rating | Оценка фотографии
[**mainViewerSavingGet**](UsersPhotosApi.md#mainViewerSavingGet) | **GET** /main/viewer/saving | Сохранение фотографии

<a name="mainCitiesGet"></a>
# **mainCitiesGet**
> List&lt;InlineResponse2002&gt; mainCitiesGet()

Получение списка городов

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UsersPhotosApi;


UsersPhotosApi apiInstance = new UsersPhotosApi();
try {
    List<InlineResponse2002> result = apiInstance.mainCitiesGet();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersPhotosApi#mainCitiesGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;InlineResponse2002&gt;**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="mainViewerGet"></a>
# **mainViewerGet**
> List&lt;InlineResponse2003&gt; mainViewerGet(city)

Получение фотографий пользователей

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UsersPhotosApi;


UsersPhotosApi apiInstance = new UsersPhotosApi();
Integer city = 56; // Integer | Код города
try {
    List<InlineResponse2003> result = apiInstance.mainViewerGet(city);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersPhotosApi#mainViewerGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **city** | **Integer**| Код города |

### Return type

[**List&lt;InlineResponse2003&gt;**](InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="mainViewerRatingGet"></a>
# **mainViewerRatingGet**
> mainViewerRatingGet(mail, token, id, rating)

Оценка фотографии

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UsersPhotosApi;


UsersPhotosApi apiInstance = new UsersPhotosApi();
String mail = "mail_example"; // String | Почта
String token = "token_example"; // String | Токен
Integer id = 56; // Integer | Идентификатор фото
Integer rating = 56; // Integer | Оценка
try {
    apiInstance.mainViewerRatingGet(mail, token, id, rating);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersPhotosApi#mainViewerRatingGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mail** | **String**| Почта |
 **token** | **String**| Токен |
 **id** | **Integer**| Идентификатор фото |
 **rating** | **Integer**| Оценка |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="mainViewerSavingGet"></a>
# **mainViewerSavingGet**
> mainViewerSavingGet(mail, token, id)

Сохранение фотографии

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UsersPhotosApi;


UsersPhotosApi apiInstance = new UsersPhotosApi();
String mail = "mail_example"; // String | Почта
String token = "token_example"; // String | Токен
Integer id = 56; // Integer | Идентификатор фото
try {
    apiInstance.mainViewerSavingGet(mail, token, id);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersPhotosApi#mainViewerSavingGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mail** | **String**| Почта |
 **token** | **String**| Токен |
 **id** | **Integer**| Идентификатор фото |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

