# LogregApi

All URIs are relative to *https://api.example.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autoPost**](LogregApi.md#autoPost) | **POST** /auto | Авторизация пользователя
[**regKeyPost**](LogregApi.md#regKeyPost) | **POST** /reg/key | Подтверждение регистрации
[**regPost**](LogregApi.md#regPost) | **POST** /reg | Регистрация пользователя

<a name="autoPost"></a>
# **autoPost**
> InlineResponse200 autoPost(login, pass)

Авторизация пользователя

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LogregApi;


LogregApi apiInstance = new LogregApi();
String login = "login_example"; // String | Логин
String pass = "pass_example"; // String | Пароль
try {
    InlineResponse200 result = apiInstance.autoPost(login, pass);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LogregApi#autoPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | **String**| Логин |
 **pass** | **String**| Пароль |

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="regKeyPost"></a>
# **regKeyPost**
> InlineResponse200 regKeyPost(mail, key)

Подтверждение регистрации

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LogregApi;


LogregApi apiInstance = new LogregApi();
String mail = "mail_example"; // String | Почта
Integer key = 56; // Integer | Код из письма
try {
    InlineResponse200 result = apiInstance.regKeyPost(mail, key);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LogregApi#regKeyPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mail** | **String**| Почта |
 **key** | **Integer**| Код из письма |

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="regPost"></a>
# **regPost**
> InlineResponse2001 regPost(login, pass, mail)

Регистрация пользователя

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LogregApi;


LogregApi apiInstance = new LogregApi();
String login = "login_example"; // String | Логин
String pass = "pass_example"; // String | Пароль
String mail = "mail_example"; // String | Почта
try {
    InlineResponse2001 result = apiInstance.regPost(login, pass, mail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LogregApi#regPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | **String**| Логин |
 **pass** | **String**| Пароль |
 **mail** | **String**| Почта |

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

