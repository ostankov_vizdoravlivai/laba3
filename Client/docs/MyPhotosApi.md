# MyPhotosApi

All URIs are relative to *https://api.example.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**mainMyDeleteGet**](MyPhotosApi.md#mainMyDeleteGet) | **GET** /main/my/delete | Сохранение фотографии
[**mainMyPutPut**](MyPhotosApi.md#mainMyPutPut) | **PUT** /main/my/put | Загрузка фотографии
[**mainMySavedGet**](MyPhotosApi.md#mainMySavedGet) | **GET** /main/my/saved | Список сохраненных фоток

<a name="mainMyDeleteGet"></a>
# **mainMyDeleteGet**
> mainMyDeleteGet(mail, token, id)

Сохранение фотографии

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.MyPhotosApi;


MyPhotosApi apiInstance = new MyPhotosApi();
String mail = "mail_example"; // String | Почта
String token = "token_example"; // String | Токен
Integer id = 56; // Integer | Идентификатор фото
try {
    apiInstance.mainMyDeleteGet(mail, token, id);
} catch (ApiException e) {
    System.err.println("Exception when calling MyPhotosApi#mainMyDeleteGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mail** | **String**| Почта |
 **token** | **String**| Токен |
 **id** | **Integer**| Идентификатор фото |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="mainMyPutPut"></a>
# **mainMyPutPut**
> mainMyPutPut(mail, token, photo)

Загрузка фотографии

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.MyPhotosApi;


MyPhotosApi apiInstance = new MyPhotosApi();
String mail = "mail_example"; // String | Почта
String token = "token_example"; // String | Токен
String photo = "photo_example"; // String | фотография
try {
    apiInstance.mainMyPutPut(mail, token, photo);
} catch (ApiException e) {
    System.err.println("Exception when calling MyPhotosApi#mainMyPutPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mail** | **String**| Почта |
 **token** | **String**| Токен |
 **photo** | **String**| фотография |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="mainMySavedGet"></a>
# **mainMySavedGet**
> List&lt;InlineResponse2003&gt; mainMySavedGet(mail, token)

Список сохраненных фоток

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.MyPhotosApi;


MyPhotosApi apiInstance = new MyPhotosApi();
String mail = "mail_example"; // String | Почта
String token = "token_example"; // String | Токен
try {
    List<InlineResponse2003> result = apiInstance.mainMySavedGet(mail, token);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MyPhotosApi#mainMySavedGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mail** | **String**| Почта |
 **token** | **String**| Токен |

### Return type

[**List&lt;InlineResponse2003&gt;**](InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

