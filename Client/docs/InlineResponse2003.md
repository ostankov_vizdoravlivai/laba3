# InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nick** | **String** | Ник владельца |  [optional]
**rating** | **Integer** | Оценка |  [optional]
**url** | **String** | Ссылка |  [optional]
**id** | **Integer** | Идентификатор картинки |  [optional]
