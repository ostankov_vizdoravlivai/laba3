# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **String** | Город |  [optional]
**kod** | **Integer** | Код |  [optional]
